import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import{LoginComponent} from './login/login.component';
import{WelcomeComponent} from './welcome/welcome.component';
import{CheckTempComponent} from './check-temp/check-temp.component';
import{CheckTemp2Component} from './check-temp2/check-temp2.component'
import {RegisterComponent} from './register/register.component'
import{ThankuComponent} from './thanku/thanku.component'
import{HistoryComponent} from './history/history.component'
import{ChangePasswordComponent} from './change-password/change-password.component'
import{HistoricDataComponent} from './historic-data/historic-data.component'
import{ForgetPassComponent}from './forget-pass/forget-pass.component'
import{MyProfileComponent } from './my-profile/my-profile.component'
import{ViewProfileComponent} from './view-profile/view-profile.component'
import{ViewHistoryComponent} from './view-history/view-history.component'
import{ViewHistoryByDateComponent} from './view-history-by-date/view-history-by-date.component'
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: LoginComponent
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'check-temp',
    component: CheckTempComponent 
  },
  {
    path: 'check-temp2',
    component: CheckTemp2Component 
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'thanku',
    component: ThankuComponent
  },
  {
    path: 'history',
    component: HistoryComponent
  },
  {
    path: 'changePass',
    component: ChangePasswordComponent
  },
  {
    path: 'historicData',
    component: HistoricDataComponent
  },
  {
    path: 'forgetpass',
    component: ForgetPassComponent
  },
  {
    path: 'myProfile',
    component: MyProfileComponent 
  },
  {
    path: 'viewProfile',
    component: ViewProfileComponent
  },
  {
    path: 'viewHistory',
    component: ViewHistoryComponent
  },
  {
    path: 'viewHistoryByDate',
    component: ViewHistoryByDateComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule { }
