import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { CheckTempComponent } from './check-temp/check-temp.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CheckTemp2Component } from './check-temp2/check-temp2.component';
import { RegisterComponent } from './register/register.component';
import { ThankuComponent } from './thanku/thanku.component';
import { HistoryComponent } from './history/history.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ForgetPassComponent } from './forget-pass/forget-pass.component';
import { HistoricDataComponent } from './historic-data/historic-data.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { ViewHistoryComponent } from './view-history/view-history.component';
import { ViewHistoryByDateComponent } from './view-history-by-date/view-history-by-date.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    CheckTempComponent,
    SidebarComponent,
    CheckTemp2Component,
    RegisterComponent,
    ThankuComponent,
    HistoryComponent,
    ChangePasswordComponent,
    ForgetPassComponent,
    HistoricDataComponent,
    MyProfileComponent,
    ViewProfileComponent,
    ViewHistoryComponent,
    ViewHistoryByDateComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
