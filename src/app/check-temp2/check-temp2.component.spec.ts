import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckTemp2Component } from './check-temp2.component';

describe('CheckTemp2Component', () => {
  let component: CheckTemp2Component;
  let fixture: ComponentFixture<CheckTemp2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckTemp2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckTemp2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
