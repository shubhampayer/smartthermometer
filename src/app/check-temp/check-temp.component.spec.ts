import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckTempComponent } from './check-temp.component';

describe('CheckTempComponent', () => {
  let component: CheckTempComponent;
  let fixture: ComponentFixture<CheckTempComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckTempComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
