import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewHistoryByDateComponent } from './view-history-by-date.component';

describe('ViewHistoryByDateComponent', () => {
  let component: ViewHistoryByDateComponent;
  let fixture: ComponentFixture<ViewHistoryByDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewHistoryByDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewHistoryByDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
